module.exports = function(grunt) {

    // load the grunt tasks
    require('load-grunt-tasks')(grunt);

    // configure the tasks
    grunt.initConfig({

        secret: grunt.file.readJSON('secret.json'),
        environments: {
            options: {
                local_path: 'build',
                current_symlink: 'current',
                deploy_path: '/var/www/html/cartodbtest/'
            },
            staging: {
                options: {
                    host: '<%= secret.staging.host %>',
                    username: '<%= secret.staging.username %>',
                    password: '<%= secret.staging.password %>',
                    port: '<%= secret.staging.port %>',
                    debug: true,
                    releases_to_keep: '3'
                }
            },
            production: {
                options: {
                    host: '<%= secret.production.host %>',
                    username: '<%= secret.production.username %>',
                    password: '<%= secret.production.password %>',
                    port: '<%= secret.production.port %>',
                    releases_to_keep: '5'
                }
            }
        },
        cachebreaker: {
            dev: {
                options: {
                    match: ['CartoDBFrontend.js', 'CartoDBFrontend.min.js'],
                },
                files: {
                    src: ['build/index.html']
                }
            }
        },
        copy: {
            build: {
                cwd: '.',
                src: [ 'js/**/*', 'index.html'],
                dest: 'build',
                expand: true
            },
            "copy-threejs": {
                cwd: '.',
                src: [ 'bower_components/threejs/build/three.min.js'],
                dest: 'build',
                expand: true
            },
            "copy-threejs-detector": {
                cwd: '.',
                src: [ 'bower_components/threejs/examples/js/Detector.js'],
                dest: 'build',
                expand: true
            },
            "copy-threejs-orbitcontrols": {
                cwd: '.',
                src: [ 'bower_components/threejs/examples/js/controls/OrbitControls.js'],
                dest: 'build',
                expand: true
            },
            "copy-jQuery": {
                cwd: '.',
                src: [ 'bower_components/jQuery/dist/jquery.min.js'],
                dest: 'build',
                expand: true
            },
            "copy-cartodb": {
                cwd: '.',
                src: [ 'bower_components/cartodb.js/cartodb.js'],
                dest: 'build',
                expand: true
            }
        },

        clean: {
            build: {
                src: [ 'build' ]
            },
            scripts: {
                src: [ 'build/js/CartoDBFrontend.js', 'build/js/DataLoader.js' ]
            },
        },

        uglify: {
            options: {
                mangle: true
            },
            "CartoDBFrontend-code": {
                files: {
                    'build/js/CartoDBFrontend.min.js': ['js/DataLoader.js', 'js/CartoDBFrontend.js']
                }
            }
        },

        dev_prod_switch: {
            options: {
                environment: 'prod',
                env_char: '#',
                env_block_dev: 'env:dev',
                env_block_prod: 'env:prod'
            },
            all: {
                files: {
                    'build/index.html': 'build/index.html'
                }
            }
        }
    });


    grunt.registerTask(
        'build',
        'Compiles all of the assets and copies the files to the build directory.',
        [ 'clean:build','copy', 'uglify', 'clean:scripts', 'dev_prod_switch', 'cachebreaker'  ]
    );

    grunt.registerTask(
        'deploy',
        'Deployment to server.',
        [ 'ssh_deploy:production'  ]
    );
};
