/**
 * Created by ro on 23/9/15.
 */

$(document).ready(function(){

    var renderer;
    var scene;
    var camera;
    var orbitControls;
    var dataLoader;
    var clock;

    init();

    function init() {

        if ( ! Detector.webgl ) Detector.addGetWebGLMessage();
        renderer = new THREE.WebGLRenderer({antialias: true});
        renderer.setSize( window.innerWidth, window.innerHeight );

        document.body.appendChild( renderer.domElement );

        scene = new THREE.Scene();

        camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.001, 10000 );
        camera.position.x = -73.9927765110962;
        camera.position.y = 40.74701838688458;
        camera.position.z = 0.013856053206329101;

        orbitControls = new THREE.OrbitControls( camera);
        orbitControls.maxDistance = 10000.0;
        orbitControls.minDistance = 0.0;
        orbitControls.enableRotate = true;
        orbitControls.enableZoom = true;
        orbitControls.object.position.copy(camera.position);

        var lightColor = new THREE.Color(1,1,1);
        var directionalLight = new THREE.DirectionalLight( lightColor );
        directionalLight.position.set( 1, 1, -1).normalize();
        scene.add(directionalLight);

        var ambientColor = new THREE.Color();
        ambientColor.r = 0.259;
        ambientColor.g = 0.259;
        ambientColor.b = 0.259;
        var ambientLight = new THREE.AmbientLight(ambientColor);
        scene.add(ambientLight);


        createScene();

        clock = new THREE.Clock(false);
        clock.start();

        animate();

        window.addEventListener('resize', onWindowResize, false);

        camera.lookAt(scene.position);
    }

    function createScene() {

        dataLoader = new DataLoader(scene);
        orbitControls.target.copy(new THREE.Vector3(-73.99068234602649, 40.74674567936186, 0));
    }

    function onWindowResize() {

        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();

        renderer.setSize(window.innerWidth, window.innerHeight);
    }

    function animate() {

        orbitControls.update();

        dataLoader.update(clock.getElapsedTime());

        requestAnimationFrame( animate );
        renderer.render( scene, camera );
    }
});
