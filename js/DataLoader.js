
var DataLoader = function(scene, blockSize) {

    this.scene = scene;
    this.blockSize = blockSize != undefined? blockSize: 100;
    this.currentOffset = 0;
    this.elapsedTimeSinceLastRequest = 0;
    this.numberOfPolygons = 0;
    this.requestsDone = 0;
};

DataLoader.prototype.addMultiPolygon = function(theGeom) {

    var numberOfPolygons = theGeom.coordinates.length;
    for (var polygonIndex = 0; polygonIndex < numberOfPolygons; polygonIndex++) {

        var numberOfPolygonSegments = theGeom.coordinates[polygonIndex].length;
        for (var polygonSegmentIndex = 0; polygonSegmentIndex < numberOfPolygonSegments; polygonSegmentIndex++) {

            var geom = new THREE.Geometry();

            var numberOfVertices = theGeom.coordinates[polygonIndex][polygonSegmentIndex].length;
            for (var vertexIndex = 0; vertexIndex < numberOfVertices; vertexIndex++) {

                var long = theGeom.coordinates[polygonIndex][polygonSegmentIndex][vertexIndex][0];
                var lat = theGeom.coordinates[polygonIndex][polygonSegmentIndex][vertexIndex][1];
                var vertex = new THREE.Vector3(long, lat, 0);
                geom.vertices.push(vertex);
            }

            var blue = new THREE.Color(0.0, 1.0, 0.0);
            var object = new THREE.Line(geom, new THREE.LineBasicMaterial({color: blue}));
            this.scene.add(object);
        }
    }
}

DataLoader.prototype.update = function(elapsedTime) {

    var self = this;

    if (this.numberOfPolygons == 0) {

        $.getJSON('https://rambo-test.cartodb.com/api/v2/sql?q=SELECT count(cartodb_id) FROM mnmappluto', function(countData) {
            $.each(countData.rows, function(key, val) {

                self.numberOfPolygons = val.count;
                self.numberOfRequests = Math.floor(self.numberOfPolygons/self.blockSize) + (self.numberOfPolygons%self.blockSize != 0? 1: 0);
                self.elapsedTimeSinceLastRequest = 101;
            });
        });
    }
    else {

        if (this.elapsedTimeSinceLastRequest > 100) {

            if (self.requestsDone < self.numberOfRequests) {

                if (self.requestsDone == self.numberOfRequests - 1) {

                    this.currentOffset = self.numberOfPolygons%self.blockSize;
                }

                var baseQuery = 'https://rambo-test.cartodb.com/api/v2/sql?q=SELECT cartodb_id,ST_AsGeoJSON(the_geom) as the_geom FROM mnmappluto limit ' + this.blockSize + ' offset ' + this.currentOffset;
                $.getJSON(baseQuery, function(data) {
                    $.each(data.rows, function(key, val) {

                        var theGeom = JSON.parse(val.the_geom);
                        if (theGeom.type == "MultiPolygon") {

                            self.addMultiPolygon(theGeom);
                        }
                    });
                });

                this.currentOffset += this.blockSize;
                self.requestsDone++;
            }

            this.elapsedTimeSinceLastRequest = 0;
        }
        else {

            this.elapsedTimeSinceLastRequest += elapsedTime;
        }
    }
};
