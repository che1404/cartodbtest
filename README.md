This is a CartoDB programming test for a developer position

- It is based on WebGL (using THREE.js rendering engine), and jQuery
- A progressive loader has been implemented in the master branch (a second approach using events in the branch called progressiveLoadEventsApproach).
- The main approach consist of requesting blocks of data to the CartoDB API, using the limit and offset parameters
- An OrbitController is used, so that zooming and rotating can be done using the mouse, and also panning using the keyboard
- The rendering time is a bit low, specially when all the geometry (more than 40000 MultiPolygons) is inside the viewing frustum. The optimization of the rendering time can be addressed in future versions 
- From all the possible data types in the GEOJson spec, only MultiPolygon data type has been considered for this demo
- The project files are hosted on a public repo on Bitbucket: https://bitbucket.org/che1404/cartodbtest
- The project dependencies have been handled using bower
- The building and deployment automatization tasks have been addressed using Grunt (build, minify, cachebreak, deploy) 
- A live version can be tested at: http://robertogarrido.com/cartodbtest/current
